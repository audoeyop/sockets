#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//solution: `perl -e 'print "AAAA"x 9 . "\xa2\x87\x04\x08"'`

int main(int argc, char **argv){

char username[32] = "";
char password[32] = "";
char server[32] = "";

if(argc != 2){
//C error message
fprintf(stderr,"\nusage: telnet <user>@<server>\n\n");
exit(0);
}


printf("\nInsecure telnet server\n");

strcpy(server,argv[1]);

while(!verify_user(username,password,server)){
printf("\nusername:");

scanf("%s",username);

printf("password:");

scanf("%s",password);
}

return 0;
}

verify_user(char * username, char * password, char * server){

//printf("username:%s\npassword:%s\n",username,password);

char full_host_name[65];

strcpy(full_host_name,server);
strcat(full_host_name,"@");
strcat(full_host_name,server);  

if(strlen(username) == 0 && strlen(password) == 0){
	return 0;
}
else if (!strcmp(username,"user") && !strcmp(password,"pass")){

login_successful();

return 1;
}
else{

login_failed();

return 0;
}

}

login_successful(){

 char command[200];
 printf("\nSuccesfully authenticated. Welcome.\n\n");

 
 printf("shell# ");
 while(scanf("%s",command)){
 	printf("\tExecuting '%s'\n",command);
	printf("shell# ");
 }
}

login_failed(){
 printf("\nInvalid username/password.\n");
}
