# Fun With Unix Sockets & More

## telnet.c
Insecure telnet server (Buffer overflow can bypass authentication)

## remote-exec
Execute remotes commands via AF_INET server/client connections

## server-client  
Basic network socket server/client connection example

##  testpid
Prints parent & child process IDs

# view-net-traffic
Another network socket connection example

