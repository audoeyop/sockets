/* testpid.c */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
 
int main (int argc, char **argv)
{
    printf("HERE %d\n", getpid());

    int i, pid;
    pid = fork();
    
    //parent
    if (pid){
        printf("PARENT %d\n", getpid());
        printf("PARENT Forking...the pid: %d\n\n", pid);
    }else{
        printf("CHILD %d\n", getpid());
        printf("CHILD Forking...the pid: %d\n\n", pid);
    }
   return 0;
}